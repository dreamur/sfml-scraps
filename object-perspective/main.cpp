#include <iostream>
#include <SFML/Graphics.hpp>

struct WallClass
{
    std:: vector<float> getSize() { return std:: vector<float> {h1, h2, x1, x2}; }
    void setSize(const std:: vector<float>& s) { h1 = s[0], h2 = s[1], x1 = s[2], x2 = s[3]; }

    float x1, x2, y1, y2, h1, h2;
    sf:: Color c;
};

void drawWall(sf:: RenderWindow& w, const WallClass& wc)
{
    sf:: ConvexShape wall(4);
    wall.setFillColor(wc.c);
    wall.setPoint(0, sf:: Vector2f(wc.x1, wc.y1 - wc.h1));
    wall.setPoint(1, sf:: Vector2f(wc.x1, wc.y1 + wc.h1));
    wall.setPoint(2, sf:: Vector2f(wc.x2, wc.y2 + wc.h2));
    wall.setPoint(3, sf:: Vector2f(wc.x2, wc.y2 - wc.h2));
    w.draw(wall);
}

int main()
{
    sf:: RenderWindow window(sf:: VideoMode(800, 600), "2.5 Dimensions");

    WallClass w1;

    w1.x1 = 300, w1.y1 = 300, w1.h1 = 20, w1.x2 = 365, w1.y2 = 300, w1.h2 = 20;
    w1.c = sf:: Color:: Red;

    while(window.isOpen())
    {
        sf:: Event e;
        while (window.pollEvent(e))
        {
            if (e.type == sf:: Event:: Closed) { window.close(); }

            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Right)) { w1.x1 -= 10; w1.x2 -= 10; }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Left)) { w1.x1 += 10; w1.x2 += 10; }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: E))
            {
                w1.y2 += 0.15;  w1.y1 -= 0.15;
                w1.h2 *= 0.99;  w1.h1 *= 1.01;

            }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Q))
            {
                w1.y2 -= 0.15;  w1.y1 += 0.15;
                w1.h2 *= 1.01;  w1.h1 *= 0.99;
            }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Up))
            {
                auto sz = w1.getSize();
                auto prev = sz;
                sz[0] *= 1.015, sz[1] *= 1.015, sz[2] *= 1.015, sz[3] *= 1.015;
                float ctr_prev = (prev[2] + prev[3]) / 2;
                float ctr_new = (sz[2] + sz[3]) / 2;
                float h_shift = ctr_new - ctr_prev;
                sz[2] -= h_shift, sz[3] -= h_shift;
                w1.setSize(sz);

            }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Down))
            {
                auto sz = w1.getSize();
                auto prev = sz;
                sz[0] *= 0.985, sz[1] *= 0.985, sz[2] *= 0.985, sz[3] *= 0.985;
                float ctr_prev = (prev[2] + prev[3]) / 2;
                float ctr_new = (sz[2] + sz[3]) / 2;
                float h_shift = ctr_new - ctr_prev;
                sz[2] -= h_shift, sz[3] -= h_shift;
                w1.setSize(sz);
            }

        }

        window.clear();
        drawWall(window, w1);
        window.display();
    }

    return 0;
}
