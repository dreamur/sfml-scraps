#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>

struct tileRegion : public sf:: Drawable
{
    public:
        void setTile(const sf:: RectangleShape& t) { tile = t; }
        sf:: RectangleShape getTile () const { return tile; }
        virtual void draw(sf:: RenderTarget& target, sf:: RenderStates states) const
        {
            target.draw(tile, states);
        }
    private:
        sf:: RectangleShape tile;
};

struct player
{
    public:
        void setImg(const sf:: RectangleShape& i) { img = i; }
        sf:: RectangleShape getImg () { return img; }
        void setPosition(const sf:: Vector2f& p) { position = p; }
        sf:: Vector2f getPosition() const { return position; }
        virtual void draw(sf:: RenderTarget& target, sf:: RenderStates states) const
        {
            target.draw(img, states);
        }
    private:
        sf:: RectangleShape img;
        sf:: Vector2f position;
};

void reset(std:: vector< std:: vector<tileRegion> >& g, player & p)
{
    sf:: RectangleShape p1Rect;
    p1Rect.setSize(sf:: Vector2f(45, 45));
    p1Rect.setPosition(0, 0);
    p.setImg(p1Rect);
    p.setPosition( sf:: Vector2f(0, 0) );

    std:: random_device rd;
    std:: mt19937 genr(rd());
    std:: gamma_distribution<double> distro(1.5, 3.2);

    unsigned yPos = 0;
    for(unsigned i = 0; i < g.size(); i++)
    {
        unsigned xPos = 0;
        for (unsigned k = 0; k < g[i].size(); k++)
        {

            sf:: RectangleShape rect;
            rect.setSize(sf:: Vector2f(50, 50));

            if (distro(genr) > 5.0) { rect.setFillColor(sf:: Color:: Red); }
            else { rect.setFillColor(sf:: Color(0, 128, 0)); }

            rect.setPosition(xPos, yPos);
            rect.setOutlineColor(sf:: Color:: Black);
            rect.setOutlineThickness(3);

            g[k][i].setTile(rect);

            xPos += 50;
        }

        yPos += 50;
    }
}

int main()
{
    std:: vector< std:: vector<tileRegion> > grid(20, std:: vector<tileRegion>(20));

    sf:: RenderWindow window(sf:: VideoMode(800, 600), "Region Detection");

    player P1;

    reset(grid, P1);

    while(window.isOpen())
    {
        sf:: Event e;
        while (window.pollEvent(e))
        {
            if (e.type == sf:: Event:: Closed) { window.close(); }

            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: R)) { reset(grid, P1); }

            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Left))
            {
                sf:: Vector2f xy = P1.getPosition();
                if (grid[(xy.x - 1) < 0 ? 0 : (xy.x - 1)][xy.y].getTile().getFillColor() != sf:: Color:: Red)
                {
                    xy.x -= 1;
                    P1.setPosition(sf:: Vector2f( (int)xy.x < 0 ? 0 : xy.x, xy.y) );

                    xy = P1.getImg().getPosition();
                    xy.x -= 50;
                    sf:: RectangleShape r = P1.getImg();
                    r.setPosition( (int)xy.x < 0 ? 0 : xy.x, xy.y );
                    P1.setImg(r);
                }
            }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Right))
            {
                sf:: Vector2f xy = P1.getPosition();
                if (grid[(xy.x + 1) > 14 ? 14 : (xy.x + 1)][xy.y].getTile().getFillColor() != sf:: Color:: Red)
                {
                    xy.x += 1;
                    P1.setPosition(sf:: Vector2f( (int)xy.x > 15 ? 15 : xy.x, xy.y) );

                    xy = P1.getImg().getPosition();
                    xy.x += 50;
                    sf:: RectangleShape r = P1.getImg();
                    r.setPosition( (int)xy.x > 750 ? 750 : xy.x, xy.y );
                    P1.setImg(r);
                }
            }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Up))
            {
                sf:: Vector2f xy = P1.getPosition();
                if (grid[xy.x][(xy.y - 1) < 0 ? 0 : (xy.y - 1)].getTile().getFillColor() != sf:: Color:: Red)
                {
                    xy.y -= 1;
                    P1.setPosition(sf:: Vector2f( xy.x, (int)xy.y < 0 ? 0 : xy.y) );

                    xy = P1.getImg().getPosition();
                    xy.y -= 50;
                    sf:: RectangleShape r = P1.getImg();
                    r.setPosition( xy.x, (int)xy.y < 0 ? 0 : xy.y );
                    P1.setImg(r);
                }
            }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Down))
            {
                sf:: Vector2f xy = P1.getPosition();
                if (grid[xy.x][(xy.y + 1) > 12 ? 12 : (xy.y + 1)].getTile().getFillColor() != sf:: Color:: Red)
                {
                    xy.y += 1;
                    P1.setPosition(sf:: Vector2f( xy.x, (int)xy.y > 11 ? 11 : xy.y) );

                    xy = P1.getImg().getPosition();
                    xy.y += 50;
                    sf:: RectangleShape r = P1.getImg();
                    r.setPosition( xy.x, (int)xy.y > 550 ? 550 : xy.y );
                    P1.setImg(r);
                }
            }
        }

        window.clear();

        for(unsigned i = 0; i < grid.size(); i++)
        {
            for (unsigned k = 0; k < grid[i].size(); k++)
            {
                grid[k][i].draw(window, sf:: RenderStates());
            }
        }
        P1.draw(window, sf:: RenderStates());
        window.display();
    }

    return 0;
}
