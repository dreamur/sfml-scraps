#include <iostream>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

struct battler : public sf:: Drawable
{
    public:
        void setBattlerImg(const sf:: RectangleShape& i) { battlerImg = i; }
        sf:: RectangleShape getBattlerImg() { return battlerImg; }
        sf:: RectangleShape getImg() { return battlerImg; }

    private:
        virtual void draw(sf:: RenderTarget& target, sf:: RenderStates states) const
        {
            target.draw(battlerImg, states);
        }
        sf:: RectangleShape battlerImg;
};

struct scene : public sf:: Drawable
{
    public:
        void initialize(const sf:: RenderWindow& rw, const int& index)
        {
            if (index == 0)
            {
                setupScene1(rw);
            }
            else if (index == 1)
            {
                setupScene2(rw);
            }
        }

    private:
        std:: vector<battler> combatants;
        sf:: RectangleShape ground, sky, menu;

        void setupScene1(const sf:: RenderWindow& rw)
        {
            battler monster, P1;
            auto dim = rw.getSize();

            sf:: RectangleShape rect;
            rect.setSize(sf:: Vector2f(45, 90));
            rect.setPosition(dim.x / 3, dim.y / 5);
            rect.setFillColor(sf:: Color(157, 7, 0));
            monster.setBattlerImg(rect);

            rect.setSize(sf:: Vector2f(90, 180));
            rect.setPosition(dim.x / 2, dim.y / 2);
            rect.setFillColor(sf:: Color:: Red);
            P1.setBattlerImg(rect);

            combatants.clear();
            combatants.emplace_back(monster);
            combatants.emplace_back(P1);

            menu.setSize( sf:: Vector2f(dim.x, dim.y / 4) );
            menu.setPosition( sf:: Vector2f(0, dim.y - (dim.y / 4)) );
            menu.setFillColor(sf:: Color(128, 128, 128, 180));

            ground.setSize( sf:: Vector2f(dim.x, dim.y) );
            ground.setPosition( sf:: Vector2f(0, 0));
            ground.setFillColor( sf:: Color(0, 128, 0) );

            sky.setSize( sf:: Vector2f(dim.x, dim.y / 4) );
            sky.setPosition( sf:: Vector2f(0, 0) );
            sky.setFillColor( sf:: Color(72, 61, 139) );
        }
        void setupScene2(const sf:: RenderWindow& rw)
        {
            battler monster, P1;
            auto dim = rw.getSize();

            sf:: RectangleShape rect;
            rect.setSize(sf:: Vector2f(145, 180));
            rect.setPosition(dim.x / 8, dim.y / 3.35);
            rect.setFillColor(sf:: Color(157, 7, 0));
            monster.setBattlerImg(rect);

            rect.setSize(sf:: Vector2f(45, 65));
            rect.setPosition(dim.x - (dim.x / 6.25), dim.y / 2.5);
            rect.setFillColor(sf:: Color:: Red);
            P1.setBattlerImg(rect);

            combatants.clear();
            combatants.emplace_back(monster);
            combatants.emplace_back(P1);

            menu.setSize( sf:: Vector2f(dim.x, dim.y / 4) );
            menu.setPosition( sf:: Vector2f(0, dim.y - (dim.y / 4)) );
            menu.setFillColor(sf:: Color(128, 128, 128, 180));

            ground.setSize( sf:: Vector2f(dim.x, dim.y) );
            ground.setPosition( sf:: Vector2f(0, 0));
            ground.setFillColor( sf:: Color(0, 128, 0) );

            sky.setSize( sf:: Vector2f(dim.x, dim.y / 5.5) );
            sky.setPosition( sf:: Vector2f(0, 0) );
            sky.setFillColor( sf:: Color(72, 61, 139) );
        }
        virtual void draw(sf:: RenderTarget& target, sf:: RenderStates states) const
        {
            target.draw(ground, states);
            target.draw(sky, states);

            for(auto fighters : combatants) { target.draw(fighters, states); }

            target.draw(menu, states);
        }
};


void reset(const sf:: RenderWindow& rw, scene& s, const int& index)
{
    s.initialize(rw, index);
}


int main()
{
    sf:: RenderWindow window(sf:: VideoMode(800, 600), "Battle Cameras");
    std:: vector<scene> scenes(2);
    unsigned index = 0;
    sf:: Clock ticker;
    ticker.restart();

    reset(window, scenes[0], index);

    while(window.isOpen())
    {
        sf:: Event e;
        while (window.pollEvent(e))
        {
            if (e.type == sf:: Event:: Closed) { window.close(); }

            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Right))
            {
                index = (index + 1) % scenes.size();
                reset(window, scenes[index], index);
            }

        }

        window.clear();
        window.draw(scenes[index]);
        window.display();
    }

    return 0;
}
