#ifndef CELL_H__
#define CELL_H__

#include <SFML/Graphics/RectangleShape.hpp>
#include "actor.h"

const int CELL_SIZE = 50;

enum class Terrain
{
    TERR_FIELD = 0,
    TERR_FOREST,
    TERR_MOUNTAIN,
    TERR_WATER
};

struct Cell
{
    int moveCost;  // cost to move into cell
    sf:: RectangleShape shapeObject;

    std:: shared_ptr<Actor> inhabitant;
    Terrain groundType;

    Cell();
    Cell(const Cell& C);
    Cell(Cell&& C);
    Cell& operator=(const Cell& C);
    Cell& operator=(Cell&& C);
    bool operator<(const Cell& other_cell);
    ~Cell();

    std:: shared_ptr<Actor>& get_cell_contents();
};

#endif
