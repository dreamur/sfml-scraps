#include "game.h"

void Game:: draw_all(sf:: RenderWindow& param_window)
{
    for(unsigned i = 0; i < grid.size(); i++)
    {
        for(unsigned k = 0; k < grid[i].size(); k++)
        {
            param_window.draw(grid[i][k].shapeObject);
        }
    }

    for(unsigned i = 0; i < pcList.size(); i++)
    {
        param_window.draw(pcList[i].get()->shapeObject);
    }

    for(unsigned i = 0; i < monList.size(); i++)
    {
        param_window.draw(monList[i].get()->shapeObject);
    }

    for(auto c : markedTilesList)
    {
        c.shapeObject.setFillColor(sf:: Color(0, 0, 0, 160));
        param_window.draw(c.shapeObject);
    }

    param_window.draw(cursor.shapeObject);

    param_window.draw(infoTray.shapeObject);
    param_window.draw(infoTray.infoText);
}

sf:: Color Game:: mix(sf:: Color param_col_source, sf:: Color param_col_mixer, double param_percent)
{
    int newRed = param_col_source.r * (1.0 - param_percent) + param_col_mixer.r * (param_percent);
    int newGrn = param_col_source.g * (1.0 - param_percent) + param_col_mixer.g * (param_percent);
    int newBlu = param_col_source.b * (1.0 - param_percent) + param_col_mixer.b * (param_percent);

    return sf:: Color(newRed, newGrn, newBlu);
}

void Game:: make_tile_list(int center_cell_y, int center_cell_x, int moveTotal)
{
	unsigned grid_height = grid.size();
	unsigned grid_width  = grid[0].size();

    for(unsigned scanner_y = 0; scanner_y < grid_height; scanner_y++)
    {
        for(unsigned scanner_x = 0; scanner_x < grid_width; scanner_x++)
        {
            if( (abs(scanner_y - center_cell_y) + abs(scanner_x - center_cell_x)) <= moveTotal )
            {
                markedTilesList.emplace_back(grid[scanner_y][scanner_x]);
            }
        }
    }
}

int Game:: screen_coord_to_grid_index(int param_coord) { return param_coord / CELL_SIZE; }

int Game:: grid_index_to_screen_coord(int param_index) { return param_index * CELL_SIZE; }

void Game:: init_actors(std:: mt19937& param_gen)
{
    std:: uniform_int_distribution<> healthDis(5, 25);
    std:: uniform_int_distribution<> statsDis(3, 8);
    std:: uniform_int_distribution<> movDis(4, 9);

    std:: uniform_int_distribution<> posXDis(0, 7);
    std:: uniform_int_distribution<> posYDis(0, 11);

    for(unsigned i = 0; i < pcList.size(); i++)
    {
        int newXindex = posXDis(param_gen);
        int newYindex = posYDis(param_gen);

		Actor tmpActor;

		tmpActor.health = healthDis(param_gen);
		tmpActor.attk = statsDis(param_gen);
		tmpActor.def = statsDis(param_gen);
		tmpActor.speed = statsDis(param_gen);
		tmpActor.mov = movDis(param_gen);

		tmpActor.name = "Player #" + std:: to_string(i);
		tmpActor.shapeObject.setRadius(24);
		tmpActor.shapeObject.setPointCount(6);
		tmpActor.shapeObject.setFillColor(sf:: Color(200, 200, 0));
		tmpActor.shapeObject.setOutlineColor(sf:: Color(125, 75, 0));
		tmpActor.shapeObject.setOutlineThickness(1);
		tmpActor.shapeObject.setPosition(grid_index_to_screen_coord(newXindex), grid_index_to_screen_coord(newYindex));

		tmpActor.team = ActorType:: ACTR_GOOD;
		tmpActor.state = ActorState:: ACT_STANDBY;

		pcList[i] = std:: make_shared<Actor> (tmpActor);

        grid[newYindex][newXindex].inhabitant = (pcList[i]);
    }

    for(unsigned i = 0; i < monList.size(); i++)
    {
        int newXindex = posXDis(param_gen);
        int newYindex = posYDis(param_gen);

		Actor tmpMon;

		tmpMon.health = healthDis(param_gen);
		tmpMon.attk = statsDis(param_gen);
		tmpMon.def = statsDis(param_gen);
		tmpMon.speed = statsDis(param_gen);
		tmpMon.mov = movDis(param_gen);

		tmpMon.name = "Monster #" + std:: to_string(i);
		tmpMon.shapeObject.setRadius(24);
		tmpMon.shapeObject.setPointCount(6);
		tmpMon.shapeObject.setOutlineColor(sf:: Color:: Yellow);
		tmpMon.shapeObject.setOutlineThickness(1);
		tmpMon.shapeObject.setFillColor(sf:: Color:: Red);
		tmpMon.shapeObject.setPosition(
			grid_index_to_screen_coord(newXindex) + SCREEN_WIDTH / 2,
			grid_index_to_screen_coord(newYindex)
			);

		tmpMon.team = ActorType:: ACTR_BAD;
		tmpMon.state = ActorState:: ACT_STANDBY;

		monList[i] = std:: make_shared<Actor> (tmpMon);

        grid[newYindex][newXindex +8].inhabitant = monList[i];
    }
}

void Game:: clear_grid()
{
	Cell tmpCell;
	for(unsigned i = 0; i < grid.size(); i++)
	{
		for(unsigned j = 0; j < grid[i].size(); j++)
		{
			grid[i].fill(tmpCell);
		}
	}
}

void Game:: init_grid(std:: mt19937& param_gen)
{
	clear_grid();

	globalState 	= GameState:: GAME_PLAYER_TURN;
    pcState			= PlayerTurnState:: PC_TRAVERSING;

    int max_rough_terrain_count = (SCREEN_WIDTH * SCREEN_HEIGHT) / 2500;    // cell_w * cell_h
    std:: uniform_int_distribution<> terrainDis(0, max_rough_terrain_count);
    std:: uniform_int_distribution<> chanceDis(1, 12);

    int loopCounter = 2;
    max_rough_terrain_count = terrainDis(param_gen);

    while(max_rough_terrain_count > 0 && loopCounter > 0)
    {
        for(unsigned i = 0; i < grid.size(); i++)
        {
            for(unsigned k = 0; k < grid[i].size(); k++)
            {
                int tmp_terrain_likelihood = chanceDis(param_gen);
                Terrain tmp_terrain_value;
                sf:: Color tmp_terrain_color;
                int move;

                if(tmp_terrain_likelihood > 8)
                {
                    int tmp_result = tmp_terrain_likelihood % 3; // three types OTHER THAN field

                    if(tmp_result +1 == 1)
                    {
                        move = 2;
                        tmp_terrain_color = sf:: Color(0, 100, 0);
                        tmp_terrain_value = Terrain:: TERR_FOREST;
                    }
                    else if(tmp_result +1 == 2)
                    {
                        move = 10;
                        tmp_terrain_color = sf:: Color(124, 32, 32);
                        tmp_terrain_value = Terrain:: TERR_MOUNTAIN;
                    }
                    else if(tmp_result +1 == 3)
                    {
                        move = 3;
                        tmp_terrain_color = sf:: Color(50, 50, 255);
                        tmp_terrain_value = Terrain:: TERR_WATER;
                    }

                }
                else
                {
                    move = 1;
                    tmp_terrain_color = sf:: Color(50, 205, 50);
                    tmp_terrain_value = Terrain:: TERR_FIELD;
                }

                int tmp_x = grid_index_to_screen_coord(k);
                int tmp_y = grid_index_to_screen_coord(i);

                grid[i][k].moveCost = move;
                grid[i][k].shapeObject.setFillColor(tmp_terrain_color);
                grid[i][k].shapeObject.setOutlineColor(sf:: Color:: Black);
                grid[i][k].shapeObject.setOutlineThickness(2);
                grid[i][k].shapeObject.setPosition(sf:: Vector2f(tmp_x, tmp_y));
                grid[i][k].shapeObject.setSize(sf:: Vector2f(CELL_SIZE, CELL_SIZE));
                grid[i][k].inhabitant = nullptr;
                grid[i][k].groundType = tmp_terrain_value;
            }
        }

        --loopCounter;
    }
}

void Game:: init_marker()
{
    cursor.shapeObject.setFillColor(sf:: Color(255, 255, 255, 0));
    cursor.shapeObject.setOutlineColor(sf:: Color:: White);
    cursor.shapeObject.setOutlineThickness(2);
    cursor.shapeObject.setPosition(sf:: Vector2f(0, 0));
    cursor.shapeObject.setSize(sf:: Vector2f(48, 48));
}

void Game:: init_textbox()
{
    infoTray.shapeObject.setFillColor(sf:: Color(50, 50, 50, 256 * 0.8));
    infoTray.shapeObject.setSize(sf:: Vector2f(SCREEN_WIDTH, SCREEN_HEIGHT * 0.25));
    infoTray.shapeObject.setPosition(0, SCREEN_HEIGHT * 0.75);

    infoTray.infoText.setFont(globalFont);
    infoTray.infoText.setString("Hello World");
    infoTray.infoText.setCharacterSize(30);
    infoTray.infoText.setPosition(25, 450);

}

void Game:: init_all()
{
    std:: random_device rd;
    std:: mt19937 gen(rd());

	markedTilesList.clear();

    if (!globalFont.loadFromFile("./fonts/chemical-reaction-brk/chemrea.ttf"))
    {
        std:: cout << "Font could not be loaded!\n";
    }

    init_grid(gen);
    init_actors(gen);
    init_marker();
    init_textbox();
}

void Game:: select_actor(const std:: shared_ptr<Actor>& param_actor)
{
    param_actor.get()->state = ActorState:: ACT_SELECTED;
    cursor.shapeObject.setOutlineColor(sf:: Color:: Yellow);
    pcState = (param_actor.get()->team == ActorType:: ACTR_GOOD ? PlayerTurnState:: PC_MANIPULATING : PlayerTurnState:: PC_SELECTED);
}

void Game:: deselect_actor(const std:: shared_ptr<Actor>& param_actor)
{
    param_actor.get()->state = ActorState:: ACT_STANDBY;
    cursor.shapeObject.setOutlineColor(sf:: Color:: White);
    pcState = PlayerTurnState:: PC_TRAVERSING;
    old_cell = nullptr;
    markedTilesList.clear();
}

void Game:: handlePCTurnInput()
{
    sf:: Vector2f tmp_pos = cursor.shapeObject.getPosition();
    unsigned nested_arrays_index = screen_coord_to_grid_index(tmp_pos.x);
    unsigned parent_array_index = screen_coord_to_grid_index(tmp_pos.y);
    std:: shared_ptr<Actor> tmp_inhabitant =
        grid[parent_array_index][nested_arrays_index].get_cell_contents();

	if(pcState == PlayerTurnState:: PC_TRAVERSING)
    {
        if(tmp_inhabitant)
        {
            if (tmp_inhabitant->team == ActorType:: ACTR_BAD || tmp_inhabitant->team == ActorType:: ACTR_NEUTRAL)
            {
                select_actor(tmp_inhabitant);
                make_tile_list(parent_array_index, nested_arrays_index, tmp_inhabitant->mov);
            }
            else if (tmp_inhabitant->team == ActorType:: ACTR_GOOD)
            {
                if(tmp_inhabitant->state == ActorState:: ACT_STANDBY)
                {
                    select_actor(tmp_inhabitant);
                    make_tile_list(parent_array_index, nested_arrays_index, tmp_inhabitant->mov);
                }
            }
            old_cell = std:: make_shared<Cell>(grid[parent_array_index][nested_arrays_index]);
        }
    }
    else if(pcState == PlayerTurnState:: PC_SELECTED)
    {
        if(tmp_inhabitant->state == ActorState:: ACT_SELECTED)
        {
            deselect_actor(tmp_inhabitant);
        }
    }
    else if(pcState == PlayerTurnState:: PC_MANIPULATING)
    {
        if(tmp_inhabitant == nullptr)
        {
            // swap values
        }
        else if(tmp_inhabitant->team == ActorType:: ACTR_GOOD && tmp_inhabitant->state == ActorState:: ACT_SELECTED)
        {
            deselect_actor(tmp_inhabitant);
        }
    }
}

bool Game:: is_attempted_coords_out_of_bounds(int col_num, int row_num)
{
    col_num = screen_coord_to_grid_index(col_num);
    row_num = screen_coord_to_grid_index(row_num);

    int outer_bound_w = screen_coord_to_grid_index(SCREEN_WIDTH);
    int outer_bound_h = screen_coord_to_grid_index(SCREEN_HEIGHT);

    if (col_num > outer_bound_w - 1) { return true; }
    else if (col_num < 0) { return true; }
    if (row_num > outer_bound_h - 1) { return true; }
    else if (row_num < 0) { return true; }

    return false;
}

void Game:: apply_highlight_to_actor(const std:: shared_ptr<Actor>& param_actor)
{
	ActorType tmp_team_name = (param_actor->team);
    sf:: Color tmp_color = (tmp_team_name == ActorType:: ACTR_GOOD ? sf:: Color(200, 200, 0) :
                      tmp_team_name == ActorType:: ACTR_BAD ? sf:: Color:: Red :
                                                              sf:: Color:: Black);

	param_actor.get()->shapeObject.setFillColor(mix(tmp_color, sf:: Color:: White, 0.5));
}
void Game:: remove_highlight_from_actor(const std:: shared_ptr<Actor>& param_actor)
{
    ActorType tmp_team_name = (param_actor->team);
    sf:: Color fill_color = (tmp_team_name == ActorType:: ACTR_GOOD ? sf:: Color(200, 200, 0) :
                      tmp_team_name == ActorType:: ACTR_BAD ? sf:: Color:: Red :
                                                              sf:: Color:: Black);

    sf:: Color border_color = (tmp_team_name == ActorType:: ACTR_GOOD ? sf:: Color(125, 75, 0) :
                    tmp_team_name == ActorType:: ACTR_BAD ? sf:: Color:: Yellow :
                                                            sf:: Color:: Black);

    param_actor.get()->shapeObject.setFillColor(fill_color);
	param_actor.get()->shapeObject.setOutlineColor(border_color);

}

void Game:: cursor_move(Direction param_dir)
{
    auto initial_pos = cursor.shapeObject.getPosition();

    int offset_y = (param_dir == Direction:: DIR_UP ? -50 : param_dir == Direction:: DIR_DOWN ? 50 : 0);
    int offset_x = (param_dir == Direction:: DIR_LEFT ? -50 : param_dir == Direction:: DIR_RIGHT ? 50 : 0);

    bool out_of_bounds = is_attempted_coords_out_of_bounds(initial_pos.x + offset_x, initial_pos.y + offset_y);

    if (!out_of_bounds)
    {
        cursor.shapeObject.setPosition(initial_pos.x + offset_x, initial_pos.y + offset_y);

        int previous_cell_x = screen_coord_to_grid_index(initial_pos.x);
        int previous_cell_y = screen_coord_to_grid_index(initial_pos.y);

        int tmpNewY = (param_dir == Direction:: DIR_UP ? previous_cell_y -1 :
						param_dir == Direction:: DIR_DOWN ? previous_cell_y +1 : previous_cell_y);
        int tmpNewX = (param_dir == Direction:: DIR_LEFT ? previous_cell_x -1 :
						param_dir == Direction:: DIR_RIGHT ? previous_cell_x +1 : previous_cell_x);

        std:: shared_ptr<Actor> old_actor =
			grid[previous_cell_y][previous_cell_x].get_cell_contents();

        if(old_actor)
			remove_highlight_from_actor(old_actor);

		std:: shared_ptr<Actor> new_actor =
	        grid[tmpNewY][tmpNewX].get_cell_contents();
        if(new_actor)
        {
            apply_highlight_to_actor(new_actor);
            std:: string tmpStr = new_actor.get()->name + "\n\n"
                                + std:: to_string(new_actor->health) + " "
                                + std:: to_string(new_actor->attk) + " "
                                + std:: to_string(new_actor->def) + " "
                                + std:: to_string(new_actor->mov);

            infoTray.infoText.setString(tmpStr);
        }
        else
        {
            Terrain tmpTerr = grid[tmpNewY][tmpNewX].groundType;
            std:: string tmpStr = (tmpTerr == Terrain:: TERR_FIELD ? "Field" :
                                   tmpTerr == Terrain:: TERR_WATER ? "Water" :
                                   tmpTerr == Terrain:: TERR_FOREST ? "Forest" : "Mountain");

            infoTray.infoText.setString(tmpStr);
        }
    }
}
