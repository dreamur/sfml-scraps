#ifndef ACTOR_H__
#define ACTOR_H__

#include <string>
#include <iostream>
#include <memory>
#include <SFML/Graphics/CircleShape.hpp>

enum class ActorType
{
    ACTR_GOOD = 0,
    ACTR_BAD,
    ACTR_NEUTRAL
};

enum class ActorState
{
    ACT_STANDBY = 0,
    ACT_SELECTED,
    ACT_INACTIVE,
    ACT_DEAD
};

struct Actor
{
    int health;
    int attk;
    int def;
    int speed;
    int mov;

    std:: string name;
    sf:: CircleShape shapeObject;
    ActorType team;
    ActorState state;

    Actor();
    Actor(const Actor& A);
    Actor(Actor&& A);
    Actor(std:: shared_ptr<Actor>& A);
    Actor& operator=(const Actor& A);
    Actor& operator=(Actor&& A);
    ~Actor();
};

#endif
