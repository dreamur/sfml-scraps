#include "cell.h"

Cell:: Cell() :
    moveCost(0),
    //shapeObject(std:: move(C.shapeObject)),
    inhabitant(nullptr),
    groundType(Terrain:: TERR_FIELD)
{}
Cell:: Cell(const Cell& C) :
    moveCost(C.moveCost),
    shapeObject(C.shapeObject),
    inhabitant(C.inhabitant),
    groundType(C.groundType)
{}
Cell:: Cell(Cell&& C) :
    moveCost(std:: move(C.moveCost)),
    shapeObject(std:: move(C.shapeObject)),
    inhabitant(std:: move(C.inhabitant)),
    groundType(std:: move(C.groundType))
{}
Cell& Cell:: operator=(const Cell& C)
{
    moveCost = C.moveCost;
    shapeObject = C.shapeObject;
    inhabitant = C.inhabitant;
    groundType = C.groundType;

	return *this;
}
Cell& Cell:: operator=(Cell&& C)
{
    moveCost = std:: move(C.moveCost);
    shapeObject = std:: move(C.shapeObject);
    inhabitant = std:: move(C.inhabitant);
    groundType = std:: move(C.groundType);

	return *this;
}
bool Cell:: operator<(const Cell& other_cell)
{
    auto leftCoords = this->shapeObject.getPosition();
    auto rightCoords = other_cell.shapeObject.getPosition();

    int x = leftCoords.x - rightCoords.x;
    int y = leftCoords.y - rightCoords.y;

    if(x > 0 && y > 0) { return false; }
    else if(x < 0 && y < 0) { return true; }
    else if(x > 0 && y < 0) { return true; }
    else { return false; }
}
Cell:: ~Cell() {}

std:: shared_ptr<Actor>& Cell:: get_cell_contents() { return inhabitant; }
