#include <SFML/Window/Event.hpp>
#include "game.h"

int main()
{
    sf:: RenderWindow window(sf:: VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "Turn Based Tactics");

    Game game_object;

    game_object.init_all();

    while(window.isOpen())
    {
        sf:: Event e;
        while (window.pollEvent(e))
        {
            if (e.type == sf:: Event:: Closed) { window.close(); }

            if (e.type == sf:: Event:: KeyPressed)
            {
                if (game_object.pcState != PlayerTurnState:: PC_SELECTED)
                {
                    if (e.key.code == sf:: Keyboard:: Right)
                    {
                        game_object.cursor_move(Direction:: DIR_RIGHT);
                    }
                    else if (e.key.code == sf:: Keyboard:: Left)
                    {
                        game_object.cursor_move(Direction:: DIR_LEFT);
                    }
                    else if (e.key.code == sf:: Keyboard:: Up)
                    {
                        game_object.cursor_move(Direction:: DIR_UP);
                    }
                    else if (e.key.code == sf:: Keyboard:: Down)
                    {
                        game_object.cursor_move(Direction:: DIR_DOWN);
                    }
                }
                if (e.key.code == sf:: Keyboard:: Enter)
                {
                    if(game_object.globalState == GameState:: GAME_PLAYER_TURN)
                    {
                        game_object.handlePCTurnInput();
                    }
                    else if(game_object.globalState == GameState:: GAME_PAUSED)
                    {
                        //handleMenuInput();    MenuState not added yet
                    }
                }
                if (e.key.code == sf:: Keyboard:: F1) { game_object.init_all(); }
				if (e.key.code == sf:: Keyboard:: Escape) { window.close(); }
            }
        }

        window.clear();

        game_object.draw_all(window);

        window.display();
    }

    return 0;
}
