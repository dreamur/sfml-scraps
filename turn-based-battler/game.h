#ifndef GAME__H_
#define GAME__H_

#include <array>
#include <vector>
#include <random>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Font.hpp>
#include "cell.h"

const int SCREEN_WIDTH = 800;
const int SCREEN_HEIGHT = 600;

using std:: array;
using array2D = array<array<Cell, 800/50>, 600/50>;

enum class GameState
{
    GAME_PLAYER_TURN = 0,
    GAME_AI_TURN,
    GAME_PAUSED,
    GAME_COMPLETE
};

enum class PlayerTurnState
{
    PC_TRAVERSING = 0,
    PC_SELECTED,
    PC_MANIPULATING
};

struct Marker
{
    sf:: RectangleShape shapeObject;
};

struct TextBox
{
    sf:: Text infoText;
    sf:: RectangleShape shapeObject;
};

enum class Direction
{
    DIR_UP = 0,
    DIR_LEFT,
    DIR_RIGHT,
    DIR_DOWN
};

struct Game
{
    std:: vector<Cell> markedTilesList;
    Marker cursor;

    GameState globalState;
    PlayerTurnState pcState;

    array2D grid;
    array<std:: shared_ptr<Actor>, 5> pcList = { nullptr, nullptr, nullptr, nullptr, nullptr };
    array<std:: shared_ptr<Actor>, 5> monList = { nullptr, nullptr, nullptr, nullptr, nullptr };

    std:: shared_ptr<Cell> old_cell;

    sf:: Font globalFont;
    sf:: Text gameText;
    TextBox infoTray;

    void draw_all(sf:: RenderWindow& param_window);
    sf:: Color mix(sf:: Color param_col_source, sf:: Color param_col_mixer, double param_percent);
    void make_tile_list(int center_cell_y, int center_cell_x, int moveTotal);

    int screen_coord_to_grid_index(int param_coord);
    int grid_index_to_screen_coord(int param_index);

    void init_actors(std:: mt19937& param_gen);
    void init_grid(std:: mt19937& param_gen);
    void init_marker();
    void init_textbox();
    void init_all();

    void clear_grid();

    void select_actor(const std:: shared_ptr<Actor>& param_actor);
    void deselect_actor(const std:: shared_ptr<Actor>& param_actor);

    void apply_highlight_to_actor(const std:: shared_ptr<Actor>& param_actor);
    void remove_highlight_from_actor(const std:: shared_ptr<Actor>& param_actor);

    void handlePCTurnInput();
    bool is_attempted_coords_out_of_bounds(int col_num, int row_num);
    void cursor_move(Direction param_dir);
};

#endif
