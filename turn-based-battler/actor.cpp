#include "actor.h"

Actor:: Actor() :
    health(0),
    attk(0),
    def(0),
    speed(0),
    mov(0),
    name(" "),
    shapeObject(sf:: CircleShape(0)),
    team(ActorType:: ACTR_NEUTRAL),
    state(ActorState:: ACT_STANDBY)
{}
Actor:: Actor(const Actor& A) :
    health(A.health),
    attk(A.attk),
    def(A.def),
    speed(A.speed),
    mov(A.mov),
    name(A.name),
    shapeObject(A.shapeObject),
    team(A.team),
    state(A.state)
{}
Actor:: Actor(Actor&& A) :
    health(std:: move(A.health)),
    attk(std:: move(A.attk)),
    def(std:: move(A.def)),
    speed(std:: move(A.speed)),
    mov(std:: move(A.mov)),
    name(std:: move(A.name)),
    shapeObject(std:: move(A.shapeObject)),
    team(std:: move(A.team)),
    state(std:: move(A.state))
{}
Actor:: Actor(std:: shared_ptr<Actor>& A)
{
    Actor tmp_a = *A.get();
    health      = tmp_a.health;
    attk        = tmp_a.attk;
    def         = tmp_a.def;
    speed       = tmp_a.speed;
    mov         = tmp_a.mov;
    name        = tmp_a.name;
    shapeObject = tmp_a.shapeObject;
    team        = tmp_a.team;
    state       = tmp_a.state;
}
Actor& Actor:: operator=(const Actor& A)
{
   health       = A.health;
   attk         = A.attk;
   def          = A.def;
   speed        = A.speed;
   mov          = A.mov;
   name         = A.name;
   shapeObject  = A.shapeObject;
   team         = A.team;
   state        = A.state;

   return *this;
}
Actor& Actor:: operator=(Actor&& A)
{
   health       = std:: move(A.health);
   attk         = std:: move(A.attk);
   def          = std:: move(A.def);
   speed        = std:: move(A.speed);
   mov          = std:: move(A.mov);
   name         = std:: move(A.name);
   shapeObject  = std:: move(A.shapeObject);
   team         = std:: move(A.team);
   state        = std:: move(A.state);

   return *this;
}
Actor:: ~Actor() {}
