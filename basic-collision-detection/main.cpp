// Collision testing with AABBs
// https://www.toptal.com/game/video-game-physics-part-ii-collision-detection-for-solid-objects


#include <iostream>
#include <SFML/Graphics.hpp>

int main()
{
    sf:: RenderWindow window(sf:: VideoMode(800, 600), "collision detection");

    sf:: RectangleShape target, goal;
    target.setSize(sf:: Vector2f(100, 50));
    target.setPosition(40, 50);
    target.setFillColor(sf:: Color:: Blue);

    sf:: Vector2f pt_t = target.getPosition();
    sf:: Vector2f sz_t = target.getSize();

    int x = 80, y = 200;

    goal.setSize(sf:: Vector2f(100, 10));
    goal.setPosition(y, x);

    while(window.isOpen())
    {
        sf:: Event e;
        while (window.pollEvent(e))
        {
            if (e.type == sf:: Event:: Closed) { window.close(); }

            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Right)) { y += 5; }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Left)) { y -= 5; }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Up)) { x -= 5; }
            if (sf:: Keyboard:: isKeyPressed(sf:: Keyboard:: Down)) { x += 5; }
        }

        goal.setPosition(y, x);

        sf:: Vector2f pt_g = goal.getPosition();
        sf:: Vector2f sz_g = goal.getSize();


        float d1x = pt_g.x - (pt_t.x + sz_t.x);
        float d1y = pt_g.y - (pt_t.y + sz_t.y);
        float d2x = pt_t.x - (pt_g.x + sz_g.x);
        float d2y = pt_t.y - (pt_g.y + sz_g.y);

        if(d1x > 0.0f || d1y > 0.0f) { goal.setFillColor(sf:: Color:: Green); }
        else if(d2x > 0.0f || d2y > 0.0f) { goal.setFillColor(sf:: Color:: Green); }
        else { goal.setFillColor(sf:: Color:: Red); }

        window.clear();
        window.draw(target);
        window.draw(goal);
        window.display();
    }

    std:: cout << pt_t.x << ' ' << pt_t.y << ' ' << sz_t.x << ' ' << sz_t.y << '\n';
    std:: cout << pt_t.x + sz_t.x << ' ' << pt_t.y + sz_t.y << '\n';

    return 0;
}
