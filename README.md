# sfml-scraps

<h3>Basic Collision Detection</h3>
Use the <strong>arrow keys</strong> to navigate. The movable rectable will change colors when a collision is detected.
Credit to <a href="https://www.toptal.com/game/video-game-physics-part-ii-collision-detection-for-solid-objects"><em>https://www.toptal.com/game/video-game-physics-part-ii-collision-detection-for-solid-objects</em></a> for the explanation

<h3>Battle Scenes</h3>
Press <strong>Right</strong> to view the different scenes/ layouts.

<h3>Map Region Detection</h3>
Use the <strong>arrow keys</strong> to navigate. Press <strong>R</strong> to generate a new map.

<h3>Object Perspective</h3>
Navigate with the <strong>arrow keys</strong>. Use <strong>Q</strong> and <strong>E</strong> to rotate the camera about the positive or negative Y-axis.
